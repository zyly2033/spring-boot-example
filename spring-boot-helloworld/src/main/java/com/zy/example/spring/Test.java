package com.zy.example.spring;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {
    public void hello() {
        log.info("Test -- hello");
    }
}
