package com.zy.example.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 手机验证码实体类
 *
 * @author zy
 * @since 2020-2-9
 */
@Data
public class SmsCode {
    /**
     * code验证码
     */
    private String code;

    /**
     * 过期时间 单位秒
     */
    private LocalDateTime expireTime;

    /**
     * 判断验证码是否过期
     * @return
     */
    public boolean isExpire() {
        return LocalDateTime.now().isAfter(expireTime);
    }

    /**
     * 构造函数
     * @param code
     * @param expireIn
     */
    public SmsCode(String code, int expireIn) {
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
    }

    /**
     * 构造函数
     * @param code
     * @param expireTime
     */
    public SmsCode(String code, LocalDateTime expireTime) {
        this.code = code;
        this.expireTime = expireTime;
    }

}
