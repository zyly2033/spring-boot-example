package com.zy.example.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

/**
 *  用户实体类
 *  Spring Security框架提供了一个基础用户接口UserDetails，该接口提供了基本的用户相关的操作，比如获取用户名/密码、
 *  用户账号是否过期和用户认证是否过期等，我们定义自己的User类时需要实现该接口。
 *
 * @author zy
 * @since 2020-2-9
 */
@Data
@NoArgsConstructor
public class User implements UserDetails {

    private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    private String id;

    /**
     * 用户登录名
     */
    private String username;

    /**
     * 用户真实姓名
     */
    private String realName;

    /**
     * 用户登录密码，用户的密码不应该暴露给客户端
     */
    @JsonIgnore
    private String password;

    /**
     * 用户创建者
     */
    private int createdBy;

    /**
     * 创建时间
     */
    private Long createdTime = System.currentTimeMillis();

    /**
     * 该用户关联的企业/区块id
     */
    private Map<String, Object> associatedResources = new HashMap<>();

    /**
     * 用户关注的企业列表
     */
    private List<String> favourite = new ArrayList<>();

    /**
     * 用户在系统中的角色列表，将根据角色对用户操作权限进行限制
     */
    private List<String> roles = new ArrayList<>();


    /**
     * 设置密码
     * @param password
     */
    public void setPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

    /**
     * 权限集合
     */
    private Collection<? extends GrantedAuthority> authorities = null;

    /**
     * 账户是否未过期
     */
    private boolean accountNonExpired = true;

    /**
     * 账户是否未锁定
     */
    private boolean accountNonLocked= true;

    /**
     * 用户凭证是否没过期，即密码是否未过期
     */
    private boolean credentialsNonExpired= true;

    /**
     * 用户是否可用
     */
    private boolean enabled= true;
}
