package com.zy.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 登陆页面
 *
 * @author zy
 * @since 2020-2-9
 */
@Controller
public class LoginController {

    /**
     * 自定义登录页面
     * @return
     */
    @RequestMapping("/login")
    public String login(){
        return "/login";
    }


    @ResponseBody
    @RequestMapping("/signout/success")
    public String signout() {
        return "退出成功，请重新登录";
    }
}
