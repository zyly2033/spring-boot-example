package com.zy.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * spring boot 错误页面配置
 *
 * @author zy
 * @since 2020-2-8
 */
@Controller
@RequestMapping("/error")
public class ErrorController {
    @RequestMapping("/403")
    public String error403(){
        return "/error/403";
    }

    @RequestMapping("/404")
    public String error404(){
        return "/error/404";
    }
}
