package com.zy.example.hook.importex;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 定义一个Registrar类
 *
 * @author zy
 * @since 2021/9/22
 */
public class BeanTest2Registrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        RootBeanDefinition beanDefinition = new RootBeanDefinition(BeanTest2.class);
        registry.registerBeanDefinition(BeanTest2.class.getName(), beanDefinition);
    }
}
