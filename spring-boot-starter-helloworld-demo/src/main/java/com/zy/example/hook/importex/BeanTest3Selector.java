package com.zy.example.hook.importex;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 定义一个ImportSelector
 *
 * @author zy
 * @since 2021/9/22
 */
public class BeanTest3Selector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{BeanTest3.class.getName()};
    }
}
