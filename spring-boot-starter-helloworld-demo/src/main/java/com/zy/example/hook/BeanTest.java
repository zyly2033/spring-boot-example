package com.zy.example.hook;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * bean生命周期相关的钩子函数
 *
 * @author zy
 * @since 2021/9/22
 */
@Component
@Slf4j
public class BeanTest implements InitializingBean, DisposableBean, BeanNameAware {
    @Value("${example.name}")
    private String name;

    /**
     * 构造函数
     */
    public BeanTest(){
        log.info("BeanTest：执行构造函数");
    }

    @Override
    public void destroy() throws Exception {
        log.info("BeanTest：Bean销毁时执行");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("BeanTest：afterPropertiesSet函数在属性name初始化之后执行，属性name=" + name);
    }

    @Override
    public void setBeanName(String name) {
        log.info("BeanTest：当前bean名称" + name);
    }
}
