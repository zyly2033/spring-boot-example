package com.zy.example.hook.importex;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Import一个ImportSelector
 *
 * @author zy
 * @since 2021/9/22
 */
@Configuration
@Import(value={BeanTest3Selector.class})
public class ImportBeanTest3 {
}
