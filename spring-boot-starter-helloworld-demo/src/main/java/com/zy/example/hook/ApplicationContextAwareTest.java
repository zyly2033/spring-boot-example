package com.zy.example.hook;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * ApplicationContextAware 获取的对象是 ApplicationContext
 *
 * @author zy
 * @since 2021/9/22
 */
@Slf4j
@Component
public class ApplicationContextAwareTest implements ApplicationContextAware {
    /*
     * 保存应用上下文
     */
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        log.info("遍历所有的BeanDefinition,此时还未实例化Bean");
        //输出所有BeanDefinition name
        for(String name:applicationContext.getBeanDefinitionNames()) {
            log.info(name);
        }
    }
}
