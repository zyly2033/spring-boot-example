package com.zy.example.hook.importex;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Import一个普通类
 *
 * @author zy
 * @since 2021/9/22
 */
@Configuration
@Import(value={BeanTest1.class})
public class ImportBeanTest1 {
}
