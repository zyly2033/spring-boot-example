package com.zy.example.hook.importex;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Import一个Registrar
 *
 * @author zy
 * @since 2021/9/22
 */
@Configuration
@Import(value={BeanTest2Registrar.class})
public class ImportBeanTest2 {
}
