package com.zy.example.hook.importex;

import lombok.extern.slf4j.Slf4j;

/**
 * 定义一个类BeanTest3
 *
 * @author zy
 * @since 2021/9/22
 */
@Slf4j
public class BeanTest3 {
    public BeanTest3(){
      log.info("BeanTest3：构造函数执行");
    }
}
