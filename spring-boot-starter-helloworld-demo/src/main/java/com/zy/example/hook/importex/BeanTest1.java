package com.zy.example.hook.importex;

import lombok.extern.slf4j.Slf4j;

/**
 * 定义一个类BeanTest1
 *
 * @author zy
 * @since 2021/9/22
 */
@Slf4j
public class BeanTest1 {
    public BeanTest1(){
      log.info("BeanTest1：构造函数执行");
    }
}
