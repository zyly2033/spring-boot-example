package com.zy.example.hook.importex;

import lombok.extern.slf4j.Slf4j;

/**
 * 定义一个类BeanTest2
 *
 * @author zy
 * @since 2021/9/22
 */
@Slf4j
public class BeanTest2 {
    public BeanTest2(){
      log.info("BeanTest2：构造函数执行");
    }
}
