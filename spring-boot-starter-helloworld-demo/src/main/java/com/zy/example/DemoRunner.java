package com.zy.example;

import com.zy.example.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 通过实现ApplicationRunner或CommandLineRunner接口，可以实现应用程序启动完成后自动运行run方法
 *
 * @author zy
 * @since 2021/9/22
 */
@Component
public class DemoRunner implements ApplicationRunner {
    @Autowired
    private HelloWorldService helloWorldService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        helloWorldService.helloWorld();
    }
}
