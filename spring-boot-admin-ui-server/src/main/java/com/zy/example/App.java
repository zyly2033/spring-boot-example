package com.zy.example;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author zy
 * @since  2020-2-8
 */
@SpringBootApplication
@EnableAdminServer
public class App {
    public static void main(String[] args){
        SpringApplication.run(App.class,args);
    }
}
