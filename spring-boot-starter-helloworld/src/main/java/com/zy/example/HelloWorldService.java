package com.zy.example;

/**
 * 编写服务类，也就是我们starter要实现的功能
 * 输出配置文件中的信息
 *
 * @author zy
 * @since 2021/9/22
 */
public class HelloWorldService {
    private HelloWorldProperties properties;

    /**
     * 构造函数
     */
    public HelloWorldService(HelloWorldProperties properties) {
        this.properties = properties;
    }

    public void helloWorld() {
        System.out.println(String.format("我的名字是%s,我今年%d岁", properties.getName(), properties.getAge()));
    }
}
