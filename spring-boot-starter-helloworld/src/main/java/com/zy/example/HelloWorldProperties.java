package com.zy.example;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 新建配置类，写好配置项和默认的配置值，指明配置项前缀
 *
 * @author zy
 * @since 2021/9/22
 */
@Data
@ConfigurationProperties(prefix = "example")
public class HelloWorldProperties {
    // 姓名
    private String name = "zy";

    // 年龄
    private Integer age = 18;
}
