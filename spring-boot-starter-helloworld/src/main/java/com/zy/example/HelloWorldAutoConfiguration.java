package com.zy.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 新建自动装配类，使用@Configuration和@Bean来进行自动装配
 *
 * @author zy
 * @since 2021/9/22
 */
@Configuration
@ConditionalOnClass(HelloWorldService.class)
@EnableConfigurationProperties(HelloWorldProperties.class)
public class HelloWorldAutoConfiguration {
    @Autowired
    private HelloWorldProperties properties;

    @Bean
    @ConditionalOnMissingBean
    public HelloWorldService starterService() {
        return new HelloWorldService(properties);
    }
}
