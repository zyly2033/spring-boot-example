package com.zy.example.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @Author: zy
 * @Description: spring security配置类
 * @Date: 2020-2-9
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * 配置拦截请求资源
     * @param http：HTTP请求安全处理
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()  //HTTP Basic认证方式
                .and()
                .authorizeRequests()  // 授权配置
                .anyRequest()  // 所有请求
                .authenticated(); // 都需要认证

    }
}